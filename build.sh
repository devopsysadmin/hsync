#!/bin/sh
rm -fR build dist && mkdir build dist
pip install --user -r requirements.txt
pyinstaller main.py -i mcsync.ico -n mcsync
