import sys
import os

def pause():
    input('Press ENTER to continue...')

def exit(message, errorcode=1):
    print(message)
    pause()
    sys.exit(errorcode)

def readfile(filename):
    with open(filename, 'r') as f:
        contents = f.read()
    return contents

def writefile(filename, contents):
    with open(filename, 'w') as f:
        f.write(contents)

def read_or_create(filename, contents=''):
    if not os.path.exists(filename):
        writefile(filename, contents)
    return readfile(filename)
        

def mkdir(dirname):
    dn = path(dirname)
    if not os.path.exists(dn):
        os.makedirs(dn)

def path(pstring):
    rpath = os.path.join(os.getenv('MCSYNC_PATH'), *pstring.split('/'))
    return rpath

