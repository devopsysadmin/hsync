from urllib.request import quote, urlretrieve, Request, urlopen
from os.path import basename, isfile
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

class Manager(object):
    def __init__(self, url):
        self.url = url

    def get(self, filename):
        url = f"{self.url}/{filename}"
        print(f"Request for {url}")
        req = Request(url=url)
        res = urlopen(req, timeout=10)
        return res.read()

    def download(self, filename, dest=None):
        url = filename if filename.startswith('http') else f"{self.url}/{quote(filename)}"
        print(f"Downloading {url}...", end='', flush=True)
        if not dest:
            dest = basename(filename)
        try:
            if not isfile(dest):
                urlretrieve(url, dest)
            print(' ok', flush=True)
        except Exception as e:
            print(' error', e)
