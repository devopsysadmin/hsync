from subprocess import Popen, PIPE
import os
import json
import re
from lib import commons
from datetime import datetime

class Profile(object):

    def __init__(self, ospath, name):
        self.name = name
        self.json = os.path.join(ospath, '.minecraft', 'launcher_profiles.json')
        self.contents = None
        self.__load()

    def __load(self):
        if not os.path.isfile(self.json):
            commons.exit(f"File {self.json} was not found. Please run minecraft at least once")
        with open(self.json, 'r') as fn:
            self.contents = json.loads(fn.read())

    def reload(self):
        self.__load()

    def save(self):
        with open(f"{self.json}", 'w') as fn:
            fn.write(json.dumps(self.contents, indent=2))

    def exists(self):
        return self.name in self.contents['profiles']

    def create(self):
        self.contents['profiles'][self.name] = dict(**self.contents['profiles']['forge'])
        self.contents['profiles'][self.name].update({
            'name' : self.name
        })
        self.save()

    def update_version(self, value):
        profileversion = self.contents['profiles'][self.name]['lastVersionId']
        if value != profileversion:
            self.contents['profiles'][self.name]['lastVersionId'] = value
            return True
        else:
            return False

    def update_memory(self, value):
        me = self.contents['profiles'][self.name]
        javaArgs = me.get('javaArgs', None)
        if javaArgs is None:
            me['javaArgs'] = f"-Xmx{value}G -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M"
            return True
        else:
            found = re.search(r'(.*)-Xmx(\d+)G(.*)', me['javaArgs'])
            if(found.group(2)) == value:
                return False
            else:
                me['javaArgs'] = ''.join([found.group(1), f"-Xmx{value}G", found.group(3)])
                self.contents['profiles'][self.name] = me
                return True

    def update_time(self):
        self.contents['profiles'][self.name]['lastUsed'] = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.00Z')
        
    def update_key(self, key, value):
        if key in self.contents['profiles'][self.name]:
            if self.contents['profiles'][self.name][key] == value:
                return False
            else:
                self.contents['profiles'][self.name][key] = value
                return True
        else:
            self.contents['profiles'][self.name][key] = value
            return True

    def update(self, remote, **kwargs):
        changes = list()
        news = remote['profile']
        changes.append(self.update_version(f"{remote['minecraft']}-forge-{remote['forge']}"))
        changes.append(self.update_memory(news.get('memory')))
        for key, value in kwargs.items():
            changes.append(self.update_key(key, value))
        if any(changes):
            self.update_time()
            self.save()
