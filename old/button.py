from tkinter import ttk

class Button(object):

    def __init__(self, window, **kwargs):
        kwargs['command'] = kwargs.get('on_click')
        kwargs.pop('on_click')
        self.button = ttk.Button (window.window, **kwargs)

    def grid(self, **kwargs):
        self.button.grid(**kwargs)
