from ttkthemes import ThemedTk

class Window(object):
    def __init__(self, **kwargs):
        window = ThemedTk(theme='arc')
        if kwargs.get('grid', None) == True:
            window.grid()
            kwargs.pop('grid')
        for k, v in kwargs.items():
            getattr(window, k)(v)
        self.window = window

    def mainloop(self):
        self.window.mainloop()

    def grid(self):
        self.window.grid()
